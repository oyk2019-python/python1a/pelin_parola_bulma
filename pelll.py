parolalar = []  # parolalar adlı liste olusturduk alınan parolalar listeye atılıcak
while True:  # kullanıcıdan sayısız işlem alsın diye sonsuz döngü(yanlış girene kadar)
    print('Bir işlem seçin')
    print('1- Parolaları Listele')  # kullanıcıdan işlem seçmesi için menü gösterdik
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')  # işlemlere baktıktan sonra kullanıcı işlem secti
    if islem.isdigit():  # islemin sayi olup olmamasını kontrol ettik
        islem_int = int(islem)  # inputla alınan her şey string sayıldıgı icin integer a cevirdik
        if islem_int not in [1,
                             2]:  # aldıgımız islem 1 veya 2 değilse bu if bloğuna girip hatalı işlem yazdırıyoruz ekrana
            print('Hatalı işlem girişi')
            continue  # yeniden islem istedigimiz yere gönderir altını okumaz
        if islem_int == 2:  # eger yeni parola kaydediyorsak yani 2 numaralı islemi sectiysek

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')  # girdi ismini aldık

            kullanici_adi = input('Kullanici Adi Girin :')  # kullanici adini aldık

            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')  # parola aldık
            eposta = input('Kayitli E-posta :')  # eposta aldık
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':  # eger kullanıcı adı girerken bosluk bıraktılarsa hata mesajı gonderiyoruz basa gönderiyoruz
                print('kullanici_adi girmediz')
                continue
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:  # 2 parolanın eşleşip eşleşmedigine bakıyoruz eslesmiyorsa printle eslesmedi diyip basa gonderiyoruz
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {  # girdi imi,kullanıcı adı vs gibi key olusturup value ları kullanıcıdan alıp dict olusturduk
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(
                yeni_girdi)  # hersey dogruysa islem 2 icin en sonunda parolayı kaydediyoruz listemize yeni girdi dictini
            continue

        elif islem_int == 1:  # eger 1.islemi secerse parolaları listeliyoruz
            alt_islem_parola_no = 0  # baslangıc durumunda bi sayaç atadık sonrasında kontrol etmek icin de kullanıcaz
            for parola in parolalar:  # tek tek parolaları ekrana basmak icin for dongusune aldık
                alt_islem_parola_no += 1  # parola varsa islemlere giricegi icin bir arttırdık varsayılan ilk durumu 0ı saglamaması ve sonraki ifleri de kontrol etmek için
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get(
                    'girdi_ismi')))  # tek tek parolanın dictteki verilerini printledi döngüyle
            alt_islem = input('Yukarıdakilerden hangisi ?: ')  # hangi kişininkini görmek istiyoruz
            if alt_islem.isdigit():
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(
                        alt_islem):  # kullanıcının girdiği 1den küçükse ve parolaların uzunlugunun 1 eksiginin  buyugu ise parola yanlıs cunku ılk durumda 0 kalmıs olur islem yapılmamıs ikinci durumda ise liste uzunlugumuzdan fazla olamaz
                    print('Hatalı parola seçimi')
                    continue  # devamını görme diyoruz
                parola = parolalar[int(
                    alt_islem) - 1]  # kullanıcıdan aldıgımız alt islemin sayıya donusumunu aldık ve bir eksigini cunku listeydi listenin o indeksi parolaya attık
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    # get ile sözlükten o degerleri cekip ekrana bastırıyoruz kullanıcının istedigi
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue

    print('Hatalı giriş yaptınız')  # diger kosulları saglamadıysa buraya atıp hatalı giris yaptırdı diyor bize